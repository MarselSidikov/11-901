package ru.itis.springbootdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.springbootdemo.models.Paper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 29.03.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaperDto {
    private Long id;
    private String title;
    private String description;

    public static PaperDto from(Paper paper) {
        return PaperDto.builder()
                .id(paper.getId())
                .title(paper.getTitle())
                .description(paper.getDescription())
                .build();
    }

    public static List<PaperDto> from(List<Paper> papers) {
        return papers.stream().map(PaperDto::from).collect(Collectors.toList());
    }
}

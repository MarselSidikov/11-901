package ru.itis.springbootdemo.services;

import ru.itis.springbootdemo.dto.PapersPage;

/**
 * 29.03.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface PapersService {
    PapersPage search(Integer size, Integer page, String query, String sort, String direction);
}

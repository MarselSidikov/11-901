package ru.itis.springbootdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.springbootdemo.models.Article;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 22.02.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ArticleDto {
    private Long id;
    private String text;
    private String authorFirstName;
    private String authorLastName;
    private Integer likesCount;

    public static ArticleDto from(Article article) {
        return ArticleDto.builder()
                .id(article.getId())
                .text(article.getText())
                .authorFirstName(article.getAuthor().getFirstName())
                .authorLastName(article.getAuthor().getLastName())
                .likesCount(article.getLikes().size())
                .build();
    }

    public static List<ArticleDto> from(List<Article> articlesOfUser) {
        return articlesOfUser.stream()
                .map(ArticleDto::from)
                .collect(Collectors.toList());
    }
}

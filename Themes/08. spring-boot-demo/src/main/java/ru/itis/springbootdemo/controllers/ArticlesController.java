package ru.itis.springbootdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.springbootdemo.dto.ArticleDto;
import ru.itis.springbootdemo.dto.ArticleForm;
import ru.itis.springbootdemo.services.ArticlesService;

/**
 * 22.02.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class ArticlesController {

    @Autowired
    private ArticlesService articlesService;

    @PostMapping("/users/{user-id}/articles/{article-id}/like")
    @ResponseBody
    public Object like(@PathVariable("user-id") Long userId, @PathVariable("article-id") Long articleId) {
        return articlesService.like(userId, articleId);
    }

    @PostMapping("/users/{user-id}/articles")
    @ResponseBody
    public ArticleDto addArticle(@PathVariable("user-id") Long userId, @RequestBody ArticleForm article) {
        return articlesService.addArticle(userId, article);
    }

    @GetMapping("/users/{user-id}/articles")
    public String getArticlesOfUser(@PathVariable("user-id") Long userId, Model model) {
        model.addAttribute("articles", articlesService.getByUser(userId));
        return "articles_page";
    }
}

package ru.itis.springbootdemo.models;

/**
 * 15.02.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public enum State {
    NOT_CONFIRMED, CONFIRMED;
}

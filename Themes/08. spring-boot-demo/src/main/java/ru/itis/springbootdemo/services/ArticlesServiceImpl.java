package ru.itis.springbootdemo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.springbootdemo.dto.ArticleDto;
import ru.itis.springbootdemo.dto.ArticleForm;
import ru.itis.springbootdemo.models.Article;
import ru.itis.springbootdemo.models.User;
import ru.itis.springbootdemo.repositories.ArticlesRepository;
import ru.itis.springbootdemo.repositories.UsersRepository;

import java.util.List;

/**
 * 22.02.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component
public class ArticlesServiceImpl implements ArticlesService {

    @Autowired
    private ArticlesRepository articlesRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public ArticleDto addArticle(Long userId, ArticleForm article) {
        User author = usersRepository.getOne(userId);

        Article newArticle = Article.builder()
                .author(author)
                .text(article.getText())
                .build();

        articlesRepository.save(newArticle);
        return ArticleDto.from(newArticle);
    }

    @Override
    public List<ArticleDto> getByUser(Long userId) {
        User user = usersRepository.getOne(userId);
        List<Article> articlesOfUser = user.getCreatedArticles();
        return ArticleDto.from(articlesOfUser);
    }

    @Override
    public ArticleDto like(Long userId, Long articleId) {
        User user = usersRepository.getOne(userId);
        Article article = articlesRepository.getOne(articleId);
        if (articlesRepository.existsByIdAndLikesContaining(articleId, user)) {
            article.getLikes().remove(user);
        } else {
            article.getLikes().add(user);
        }
        articlesRepository.save(article);
        return ArticleDto.from(article);
    }
}

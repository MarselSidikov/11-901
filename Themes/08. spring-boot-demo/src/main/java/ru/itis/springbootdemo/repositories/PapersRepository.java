package ru.itis.springbootdemo.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.springbootdemo.models.Paper;

/**
 * 29.03.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface PapersRepository extends JpaRepository<Paper, Long> {

    @Query("select paper from Paper paper where " +
            "(:q = 'empty' or UPPER(paper.description) like UPPER(concat('%', :q, '%')) " +
            "              or UPPER(paper.title) like UPPER(concat('%', :q, '%')))")
    Page<Paper> search(@Param("q") String q, Pageable pageable);
}

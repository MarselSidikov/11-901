package ru.itis.springbootdemo.services;

import ru.itis.springbootdemo.dto.ArticleDto;
import ru.itis.springbootdemo.dto.ArticleForm;
import ru.itis.springbootdemo.models.Article;

import java.util.List;

/**
 * 22.02.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ArticlesService {
    ArticleDto addArticle(Long userId, ArticleForm article);

    List<ArticleDto> getByUser(Long userId);

    ArticleDto like(Long userId, Long articleId);
}

package ru.itis.springbootdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springbootdemo.models.Article;
import ru.itis.springbootdemo.models.User;

/**
 * 22.02.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ArticlesRepository extends JpaRepository<Article, Long> {
    boolean existsByIdAndLikesContaining(Long articleId, User user);
}

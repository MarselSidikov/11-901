package ru.itis.springbootdemo.dto;

import lombok.Data;

/**
 * 22.02.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
public class ArticleForm {
    private String text;
}

package ru.itis.springbootdemo.models;

/**
 * 01.03.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public enum Role {
    USER, ADMIN
}

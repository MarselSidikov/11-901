package ru.itis.springbootdemo.services;

/**
 * 15.02.2021
 * 08. spring-boot-demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface MailsService {
    void sendEmailForConfirm(String email, String code);
}

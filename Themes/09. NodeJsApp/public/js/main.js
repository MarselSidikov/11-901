function getTeachers() {
    $.get("http://localhost:8080/teachers", function (data) {
        let html = '';

        for (let i = 0; i < data.length; i++) {
            html += '<tr>' +
                        '<td>' + data[i].id + '</td>' +
                        '<td>' + data[i].firstName + '</td>' +
                        '<td>' + data[i].lastName + '</td>' +
                    '</tr>'
        }

        $('#table_header').after(html);
    })
}
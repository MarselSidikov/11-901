package ru.itis.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * 26.10.2020
 * 05. Skeleton
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@Builder
public class UserForm {
    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Length(min = 3, max = 30)
    private String firstName;

    @NotEmpty
    @Length(min = 3, max = 30)
    private String lastName;

    @NotEmpty
    @Length(min = 8)
    private String password;
}

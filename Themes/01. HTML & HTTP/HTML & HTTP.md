# HTML & HTTP

* Сервер - приложение, которое обрабатывает запросы клиентов. Либо отдельная машина (хостинг). Имеет свой выделенный белый IP-адрес, и, возможно, привязанное доменное имя.

* Клиент - браузер, мобильное приложение, Desktop-клиент. Отправляет запросы серверу и получает ответы. Не имеет выделенного IP-адреса.

## HTTP-протокол

### HTTP-запрос

* URL - протокол, домен, ресурс, параметры

* Тело запроса

* Заголовки запроса
		
		* `Content-type`
		* `cookie` 

### HTTP-ответ

* Статус ответа

		* `200`
		* `500`
		* `404`

* Тело ответа

* Заголовки ответа

		* `Set-cookie`
		* `Content-type`

## HTML

* Тег - `<form>` для отправки get-post методов.
